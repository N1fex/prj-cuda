import $ from 'jquery'
import progbar from 'progressbar.js'

$(document).ready(function(){
    let circle1 = new progbar.Circle('#progressbar_1', {
        color: '#30bae7',
        duration: 2000,
        easing: 'easeInOut',
        strokeWidth: 8,
        trailColor: '#dfe8ed',
        trailWidth: 8,
        text: {
            value: 0
        }	
    });

    let circle2 = new progbar.Circle('#progressbar_2', {
        color: '#d74680',
        duration: 2000,
        easing: 'easeInOut',
        strokeWidth: 8,
        trailColor: '#dfe8ed',
        trailWidth: 8,
        text: {
            value: 0
        }	
    });

    let circle3 = new progbar.Circle('#progressbar_3', {
        color: '#15c7a8',
        duration: 2000,
        easing: 'easeInOut',
        strokeWidth: 8,
        trailColor: '#dfe8ed',
        trailWidth: 8,
        text: {
            value: 0
        }
    });

    let circle4 = new progbar.Circle('#progressbar_4', {
        color: '#eb7d4b',
        duration: 2000,
        easing: 'easeInOut',
        strokeWidth: 8,
        trailColor: '#dfe8ed',
        trailWidth: 8,
        text: {
            value: 0
        }
    });

    // валидация touch__form
    $('.touch__form input, .touch__form textarea').keyup(function(){
        let red_star = $(this).parent().find('.red-star');
        $(this).val() != '' ? red_star.addClass('hide') : red_star.removeClass('hide');
    })
    
    // SCROLL
    // опции для обсервера
    var optionsScroll = {
        threshold: 0.5
    }

    // функция, которая будет выполняться при появлении элемента в зоне просмотра
    var callback = function(entries, observer) { 
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                circle1.animate(0.9,{
                    step: function(){
                        circle1.setText(Math.floor(circle1.value()*100) + '<span class="percent">%</span>')
                    }
                })

                circle2.animate(0.75,{
                    step: function(){
                        circle2.setText(Math.floor(circle2.value()*100) + '<span class="percent">%</span>')
                    }
                })
                
                circle3.animate(0.7,{
                    step: function(){
                        circle3.setText(Math.floor(circle3.value()*100) + '<span class="percent">%</span>')
                    }
                })

                circle4.animate(0.85,{
                    step: function(){
                        circle4.setText(Math.floor(circle4.value()*100) + '<span class="percent">%</span>')
                    }
                })

                observer.disconnect();
            }
        });
    };

    // задаем обсервер
    var observer = new IntersectionObserver(callback, optionsScroll);
    
    // создаем элемент, за которым будем следить
    var target = document.querySelector('.skills__components');
    observer.observe(target);
})

