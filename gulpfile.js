var gulp = require('gulp'), // Подключаем пакет gulp из папки node_modules
  watch = require('gulp-watch'), // Следит за изменением файлов и директорий
  prefixer = require('gulp-autoprefixer'), // Добавляет префиксы к CSS свойствам
  sass = require('gulp-sass'), // Ускоряет и упрощает процесс разработки благодаря вложенности CSS правил
  cssmin = require('gulp-minify-css'), // Минимизирует и объединяет CSS-скрипты
  imagemin = require('gulp-imagemin'), // Сжимает изображения
  pngquant = require('imagemin-pngquant'), // Сжимает png-изображения с потерями
  rimraf = require('rimraf'), // Предоставляет функциональность команды rm -rf
  browserSync = require("browser-sync"), // Обеспечивает автообновление браузера при сохранении изменённых файлов
  cache = require('gulp-cache'), // Обеспечивает кэширование результатов обработки файлов
  plumber = require("gulp-plumber"), // Формирует вывод об ошибке
  runSequence = require('run-sequence'), // Запускает последовательность заданий gulp в указанном порядке
  browserify = require('browserify'), // Открывает в JavaScript файл и следует дереву зависимостей, указанных в нем с помощью require, а затем собирает их и исходный скрипт в новый файл
  source = require('vinyl-source-stream'), // Преобразует читаемый поток, который мы получаем от browserify, в виниловый поток, который ожидает получить gulp
  sourcemaps = require('gulp-sourcemaps'), // Эти файлы - "карты ресурсов" "запоминают", какие стили в каких исходных sass-файлах находятся
  babelify = require('babelify'), // Содержит babel внутри, чтобы иметь возможность преобразовать код ES6 в ES5 с помощью browserify
  reload = browserSync.reload; // Используется для обновления страницы

var path = {
  build: { // Указываем куда складывать готовые после сборки файлы
    html: 'build/',
    js: 'build/js/',
    css: 'build/css/',
    img: 'build/img/',
    fonts: 'build/fonts/'
  },
  src: { // Указываем пути откуда брать исходники
    html: 'src/*.html',
    js: 'src/js/main.js',
    style: 'src/style/app.scss',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  watch: { // Указываем за изменением каких файлов хотим наблюдать
    html: 'src/**/*.html',
    js: 'src/js/**/*.js',
    style: 'src/style/**/*.*',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  clean: './build'
};

// Создаём переменную с настройками нашего dev сервера:
var config = {
  server: {
    baseDir: "./build" // Указываем корневую папку для локального сервера
  },
  notify: false, // Отключаем уведомления
  browser: "chrome" // Указываем браузер
};

var onError = function (err) {
  console.log(err);
  this.emit('end');
};

// Tasks
gulp.task('html', function () {
  return gulp.src(path.src.html)
    .pipe(plumber({errorHandler: onError}))
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({stream: true}));
});

gulp.task('js', function () {
  return browserify('./src/js/main.js')
    .transform(babelify)
    .bundle()
    .pipe(source('main.js'))
    .pipe(gulp.dest('./build/js'))
    .pipe(reload({stream: true}));
});

gulp.task('style', function () {
  return gulp.src(path.src.style)
    .pipe(plumber({errorHandler: onError}))
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: ['src/style/'],
      outputStyle: 'compressed',
      sourceMap: true,
      errLogToConsole: true
    }))
    .pipe(prefixer()) // Добавляем вендорные префиксы
    .pipe(cssmin())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({stream: true}));
});

gulp.task('image', function () {
  return gulp.src(path.src.img) // Выбираем наши картинки
    .pipe(plumber({errorHandler: onError}))
    .pipe(cache(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()],
      interlaced: true
    })))
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({stream: true}));
});

gulp.task('fonts', function() {
  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', ['html', 'js', 'style', 'fonts', 'image']);

gulp.task('watch', function(){
  watch([path.watch.html], function() {
    gulp.start('html');
  });
  watch([path.watch.style], function() {
    gulp.start('style');
  });
  watch([path.watch.js], function() {
    gulp.start('js');
  });
  watch([path.watch.img], function() {
    gulp.start('image');
  });
  watch([path.watch.fonts], function() {
    gulp.start('fonts');
  });
});

gulp.task('webserver', function () {
  return browserSync(config);
});

gulp.task('clean', function (cb) {
  rimraf(path.clean, cb);
});

gulp.task('clear', function () {
  return cache.clearAll();
});

const tasksSequence = ['build'];

gulp.task('default', cb => {
  runSequence(...tasksSequence, 'watch', 'webserver', cb);
});